<?php

/**
 * @file
 * entityform_noe.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function entityform_noe_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_ef_acknowledgement|entityform|candidate_request_form|form';
  $field_group->group_name = 'group_ef_acknowledgement';
  $field_group->entity_type = 'entityform';
  $field_group->bundle = 'candidate_request_form';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_ef_multigroup';
  $field_group->data = array(
    'label' => 'Acknowledgement',
    'weight' => '5',
    'children' => array(
      0 => 'field_ef_acknowledgement_email',
      1 => 'field_ef_acknowledgement_name',
      2 => 'field_ef_acknowledgement_info',
    ),
    'format_type' => 'multipage',
    'format_settings' => array(
      'formatter' => 'no-start',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-ef-acknowledgement',
        'required_fields' => 1,
      ),
    ),
  );
  $field_groups['group_ef_acknowledgement|entityform|candidate_request_form|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_ef_ind_coach|entityform|candidate_request_form|form';
  $field_group->group_name = 'group_ef_ind_coach';
  $field_group->entity_type = 'entityform';
  $field_group->bundle = 'candidate_request_form';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_ef_multigroup';
  $field_group->data = array(
    'label' => 'Induction Coach Information',
    'weight' => '3',
    'children' => array(
      0 => 'field_ef_ind_coach_selected',
      1 => 'field_ef_ind_coach_first_name',
      2 => 'field_ef_ind_coach_last_name',
      3 => 'field_ef_ind_coach_school_site',
      4 => 'field_ef_ind_coach_email',
      5 => 'field_ef_ind_coach_setting',
      6 => 'field_ef_ind_coach_grade_level',
      7 => 'field_ef_ind_coach_subject',
      8 => 'field_ef_ind_coach_cred_info',
      9 => 'field_ef_ind_coach_cred_term',
      10 => 'field_ef_ind_coach_doc_title',
      11 => 'field_ef_ind_coach_cred_exp_date',
    ),
    'format_type' => 'multipage',
    'format_settings' => array(
      'formatter' => 'no-start',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-ef-ind-coach',
        'required_fields' => 1,
      ),
    ),
  );
  $field_groups['group_ef_ind_coach|entityform|candidate_request_form|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_ef_multigroup|entityform|candidate_request_form|form';
  $field_group->group_name = 'group_ef_multigroup';
  $field_group->entity_type = 'entityform';
  $field_group->bundle = 'candidate_request_form';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Multipage Group',
    'weight' => '1',
    'children' => array(
      0 => 'group_ef_pt_info',
      1 => 'group_ef_pt_credential_info',
      2 => 'group_ef_ind_coach',
      3 => 'group_ef_acknowledgement',
    ),
    'format_type' => 'multipage-group',
    'format_settings' => array(
      'label' => 'Multipage Group',
      'instance_settings' => array(
        'classes' => 'group-ef-multigroup field-group-multipage-group',
        'page_header' => '1',
        'page_counter' => '1',
        'move_button' => '1',
        'move_additional' => '1',
      ),
    ),
  );
  $field_groups['group_ef_multigroup|entityform|candidate_request_form|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_ef_pt_credential_info|entityform|candidate_request_form|form';
  $field_group->group_name = 'group_ef_pt_credential_info';
  $field_group->entity_type = 'entityform';
  $field_group->bundle = 'candidate_request_form';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_ef_multigroup';
  $field_group->data = array(
    'label' => 'Participating Teacher Credential Information',
    'weight' => '2',
    'children' => array(
      0 => 'field_ef_credential_origin',
      1 => 'field_ef_university',
      2 => 'field_ef_document_title',
      3 => 'field_ef_credential_directions',
      4 => 'field_ef_pt_cred_exp_date',
      5 => 'field_ef_pt_credential_term',
    ),
    'format_type' => 'multipage',
    'format_settings' => array(
      'formatter' => 'no-start',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-ef-pt-credential-info',
        'required_fields' => 1,
      ),
    ),
  );
  $field_groups['group_ef_pt_credential_info|entityform|candidate_request_form|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_ef_pt_info|entityform|candidate_request_form|form';
  $field_group->group_name = 'group_ef_pt_info';
  $field_group->entity_type = 'entityform';
  $field_group->bundle = 'candidate_request_form';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_ef_multigroup';
  $field_group->data = array(
    'label' => 'Participating Teacher Information',
    'weight' => '1',
    'children' => array(
      0 => 'field_ef_email',
      1 => 'field_ef_district',
      2 => 'field_ef_school',
      3 => 'field_ef_induction_program',
      4 => 'field_ef_induction_status',
      5 => 'field_ef_transfer_program',
      6 => 'field_ef_hire_date',
      7 => 'field_ef_first_name',
      8 => 'field_ef_last_name',
      9 => 'field_ef_ssn',
      10 => 'field_ef_teaching_experience',
      11 => 'field_ef_gender',
      12 => 'field_ef_race',
      13 => 'field_ef_date_of_birth',
      14 => 'field_ef_personal_email',
      15 => 'field_ef_home_state',
      16 => 'field_ef_home_city',
      17 => 'field_ef_home_zip',
      18 => 'field_ef_grade_level',
      19 => 'field_ef_subject',
      20 => 'field_ef_site_admin',
      21 => 'field_ef_site_admin_email',
      22 => 'field_ef_instructions',
      23 => 'field_ef_home_address',
      24 => 'field_ef_primary_phone',
    ),
    'format_type' => 'multipage',
    'format_settings' => array(
      'label' => 'Participating Teacher Information',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => 'group-ef-pt-info',
        'description' => '',
      ),
      'formatter' => 'start',
    ),
  );
  $field_groups['group_ef_pt_info|entityform|candidate_request_form|form'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Acknowledgement');
  t('Induction Coach Information');
  t('Multipage Group');
  t('Participating Teacher Credential Information');
  t('Participating Teacher Information');

  return $field_groups;
}
